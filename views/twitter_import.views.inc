<?php

/**
 * @file
 * This file has some views functions
 */

/**
 * Implements hook_views_data().
 */
function twitter_import_views_data() {
  $data['twitter_import']['table']['group'] = t('Twitter Import');

  $data['twitter_import']['table']['base'] = array(
    'field' => 'id',
    'title' => 'Imported tweets',
    'help' => 'Tweets imported from twitter',
  );

  $data['twitter_import']['id'] = array(
    'title' => t('ID'),
    'help' => t('The unique ID'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['twitter_import']['tweet_id'] = array(
    'title' => t('Tweet ID'),
    'help' => t('The ID from twitter'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['twitter_import']['user'] = array(
    'title' => t('Twitter user'),
    'help' => t('The user from twitter'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['twitter_import']['tweet'] = array(
    'title' => t('Tweet'),
    'help' => t('The actual tweet'),
    'field' => array(
      'handler' => 'twitter_import_handler_field_tweet',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['twitter_import']['created'] = array(
    'title' => t('Post date'),
    'help' => t('The post date from twitter'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['twitter_import']['data'] = array(
    'title' => t('Extra data'),
    'help' => t('Some extra tweet data'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['twitter_import']['status'] = array(
    'title' => t('Published status'),
    'help' => t('If the tweet is published or not'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
  );

  $data['twitter_import']['deleted'] = array(
    'title' => t('Deleted status'),
    'help' => t('If the tweet is deleted or not'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Deleted'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
  );

  $data['twitter_import']['source'] = array(
    'title' => t('Twitter source'),
    'help' => t('The source of the tweet'),
    'field' => array(
      'handler' => 'twitter_import_handler_field_source',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['twitter_import']['fid'] = array(
    'title' => t('Picture'),
    'help' => t('The profile picture of the tweeter'),
    'field' => array(
      'handler' => 'twitter_import_handler_field_fid',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  return $data;
}