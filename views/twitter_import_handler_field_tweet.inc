<?php

/**
 * @file
 * The handler for the tweet field
 */
class twitter_import_handler_field_tweet extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();

    $options['display_with_links'] = array('default' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['display_with_links'] = array(
      '#title' => t('Convert hastags, @ts and hrefs to links'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_with_links']),
    );
    parent::options_form($form, $form_state);
  }

  function render($values) {
    $value = $this->get_value($values);
    if (!empty($this->options['display_with_links'])) {
      return twitter_import_twitterify($value);
    }
    else {
      return $this->sanitize_value($value);
    }
  }
}