<?php

/**
 * @file
 * The handler for the image field
 */
class twitter_import_handler_field_fid extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['user'] = 'user';
  }

  function render($values) {
    $value = $this->get_value($values);
    $user = $values->{$this->aliases['user']};
    if (!empty($value)) {
      $file = file_load($value);
      $filepath = file_create_url($file->uri);
      $img = theme('image', array(
        'path' => $filepath,
        'alt' => $user,
      ));
      return $img;
    }

    return $value;
  }
}
