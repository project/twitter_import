<?php

/**
 * @file
 * This file holds the settings page & functions
 */
function _twitter_import_settings() {
  $form = array();
  $form['twitter_import_term'] = array(
    '#title' => t('Term to search on'),
    '#type' => 'textfield',
    '#default_value' => variable_get('twitter_import_term', ''),
     '#description' => t('Visit http://search.twitter.com/operators to see the search operators'),
  );
  $form['twitter_import_user'] = array(
    '#title' => t('User to import tweets from'),
    '#type' => 'textfield',
    '#default_value' => variable_get('twitter_import_user', ''),
    '#description' => t('Insert each user seperated by a comma. Don\'t use spaces between the user and the comma.'),
  );
  $form['twitter_import_term_safe_user'] = array(
    '#title' => t('When searching on a term, these user are considered safe and their tweets are published'),
    '#type' => 'textarea',
    '#default_value' => variable_get('twitter_import_term_safe_user', ''),
    '#description' => t('Insert each user seperated by a comma. Don\'t use spaces between the user and the comma.'),
  );
  $form['twitter_manual_import'] = array(
    '#markup' => '<p>' . l(t('Manually import tweets'), TWITTER_IMPORT_IMPORTPATH) . '</p>',
  );
  return system_settings_form($form);
}

function _twitter_import_manual() {
  _twitter_import_import_terms();
  _twitter_import_import_fromuser();

  drupal_goto(TWITTER_IMPORT_CONFIGPATH);
}

function twitter_import_status_delete() {
  $header = array(
    array(
      'data' => 'Tweet',
      'field' => 'tweet',
    ),
    array(
      'data' => 'Created',
      'field' => 'created',
      'sort' => 'desc',
    ),
    array(
      'data' => '(Un)publish',
      'field' => 'status',
    ),
    'Delete',
  );

  $query = db_select('twitter_import', 'ti')
  ->extend('PagerDefault')
  ->limit(20)
  ->extend('TableSort')
  ->orderByHeader($header)
  ->condition('ti.deleted', 0)
  ->fields('ti', array(
      'tweet',
      'created',
      'status',
      'id',
    ));
  $result = $query->execute();
  $rows = array();
  foreach ($result AS $row) {
    if ($row->status == 1) {
      $status = l(t('Unpublish tweet'), TWITTER_IMPORT_PUBLISHPATH . '/' . $row->id . '/0');
    }
    else {
      $status = l(t('Publish tweet'), TWITTER_IMPORT_PUBLISHPATH . '/' . $row->id . '/1');
    }

    $rows[] = array('data' => array(
      truncate_utf8($row->tweet, 120, FALSE, TRUE),
      format_date($row->created, 'custom', 'd/m/Y H:i'),
      $status,
      l(t('Delete tweet'), TWITTER_IMPORT_DELETEPATH . '/' . $row->id),
    ));
  }

  $ret = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'sticky' => TRUE,
    'empty' => t('No tweets imported'),
  ));

  $ret .= theme('pager',
    array(
      'tags' => array()
    )
  );

  return $ret;
}

function twitter_import_status_delete_tweet() {
  $args = func_get_args();
  db_update('twitter_import')
  ->fields(array(
    'deleted' => 1,
  ))
  ->condition('id', $args[0])
  ->execute();
  drupal_set_message(t('Tweet removed.'));
  drupal_goto(TWITTER_IMPORT_STATUSPATH);
}

function twitter_import_status_publish_tweet() {
  $args = func_get_args();
  db_update('twitter_import')
  ->fields(array(
    'status' => $args[1],
  ))
  ->condition('id', $args[0])
  ->execute();

  switch ($args[1]) {
    case '0':
      drupal_set_message(t('Tweet unpublished.'));
      break;
    case '1':
      drupal_set_message(t('Tweet published.'));
      break;
  }
  drupal_goto(TWITTER_IMPORT_STATUSPATH);
}
